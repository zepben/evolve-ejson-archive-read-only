**WARNING: FOR DEVELOPMENT USE ONLY**

# ejsonbend
Ejsonbend is a client for Zepben's postbox service. Utilising postbox's grpc+protobuf interface it retrieves a network from postbox and converts it to ejson.

## Set up
Install is only supported for development purposes. In a virtualenv:

    pip install -e .
   
## Usage
Simply run `network.py`:

    python src/ejsonbend/network.py
    
By default the client will connect on `localhost:50051` to a locally running insecure postbox server. SSL is currently not supported.
