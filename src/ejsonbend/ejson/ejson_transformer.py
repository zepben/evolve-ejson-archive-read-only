"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import Iterable
from ejsonbend.ejson import EjsonConnection, EjsonElement


class EjsonTransformer(EjsonElement):

    def __init__(self, id: str, vector_group: str, v_base: list, z: list, z0: list = None, cons: Iterable[EjsonConnection] = None,
                 in_service: bool = True, nom_turns_ratio: float = None, y_bus=None, taps: dict = None,
                 tap: list = None, tap_side: str = 'primary', s_rated: float = None, user_data: dict = None):
        super().__init__(id, cons, in_service, user_data)
        self.v_base = v_base
        self.vector_group = vector_group
        self.z = z
        self.z0 = z0
        self.nom_turns_ratio = nom_turns_ratio
        self.y_bus = y_bus
        self.taps = taps
        self.tap = tap
        self.tap_side = tap_side
        self.s_rated = s_rated
        self.element_type = 'Transformer'
        self.ejson[self.element_type] = {}

    def validate(self):
        """TODO"""
        pass

    def to_ejson_dict(self):
        self.ejson[self.element_type]["v_winding_base"] = self.v_base
        self.ejson[self.element_type]["vector_group"] = self.vector_group.lower()
        self.ejson[self.element_type]["z"] = self.z
        self.ejson[self.element_type]["tap_side"] = self.tap_side

        if self.z0 is not None:
            self.ejson[self.element_type]["z0"] = self.z0

        if self.nom_turns_ratio is not None:
            self.ejson[self.element_type]["nom_turns_ratio"] = self.nom_turns_ratio

        if self.taps is not None:
            self.ejson[self.element_type]["taps"] = self.taps

        if self.tap is not None:
            self.ejson[self.element_type]["tap"] = self.tap

        if self.s_rated is not None:
            self.ejson[self.element_type]["s_rated"] = self.s_rated

        self.prepare_ejson()
        return self.ejson
