"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import List


class EjsonNode(object):

    def __init__(self, id: str, phases: List = None, v_base: float = None, dc: bool = False, lat_long = None,
                 xy=None, customer_ids: list = None, user_data: dict = None):
        super().__init__()
        self.obj_id = id
        self.phases = phases
        self.v_base = v_base
        self.lat_long = lat_long
        self.dc = dc
        self.xy = xy
        self.customer_ids = customer_ids
        self.user_data = user_data

    def default(self, o):
        self.to_ejson_dict()

    def validate(self):
        """TODO"""
        pass

    def to_ejson_dict(self):
        ejson = {
                    "Node": {
                        "phs": self.phases,
                        "v_base": self.v_base,
                    }
                }

        if self.dc:
            ejson['Node']['dc'] = self.dc

        if self.lat_long is None:
            ejson['Node']['lat_long'] = [0.0, 0.0]
        else:
            ejson['Node']['lat_long'] = self.lat_long

        if self.xy:
            ejson['Node']['xy'] = self.xy

        if self.customer_ids is not None:
            ejson['Node']['customer_ids'] = self.customer_ids

        if self.user_data is not None:
            ejson['Node']['user_data'] = self.user_data

        return ejson

