"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import Iterable
from ejsonbend.ejson import EjsonConnection, EjsonElement


class EjsonLine(EjsonElement):

    def __init__(self, id: str, i_max: float, length: float, z: list, z0: list, cons: Iterable[EjsonConnection] = None,
                 in_service: bool = True, y_bus=None, b_chg=None, user_data: dict = None):
        super().__init__(id, cons, in_service, user_data)
        self.i_max = i_max
        self.length = length
        self.z = z
        self.z0 = z0
        self.y_bus = y_bus
        self.b_chg = b_chg
        self.element_type = 'Line'
        self.ejson[self.element_type] = {}

    def validate(self):
        """TODO"""
        pass

    def to_ejson_dict(self):
        if self.i_max is not None:
            self.ejson[self.element_type]["i_max"] = self.i_max
        self.ejson[self.element_type]["z"] = self.z
        self.ejson[self.element_type]["z0"] = self.z0
        self.ejson[self.element_type]["length"] = self.length
        if self.y_bus is not None:
            self.ejson[self.element_type]["y_bus"] = self.y_bus

        if self.b_chg:
            self.ejson[self.element_type]["b_chg"] = self.b_chg

        self.prepare_ejson()
        return self.ejson
