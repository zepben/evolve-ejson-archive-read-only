"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import List, Tuple
from ejsonbend.ejson import EjsonConnection, EjsonElement


class EjsonLoad(EjsonElement):

    def __init__(self, id: str, wiring: str, cons: List[EjsonConnection] = None,
                 in_service: bool = True, s_nom: Tuple[float, float] = None, user_data: dict = None):
        super().__init__(id, cons, in_service, user_data)
        self.wiring = wiring
        self.cons = cons
        self.s_nom = s_nom
        self.element_type = 'Load'
        self.ejson[self.element_type] = {}

    def validate(self):
        """TODO"""
        pass

    def to_ejson_dict(self):
        if self.wiring:
            if self.wiring.lower() in ('delta', 'wye'):
                self.ejson[self.element_type]["wiring"] = self.wiring.lower()

        if self.s_nom is not None:
            self.ejson[self.element_type]['s_nom'] = self.s_nom

        if self.user_data is not None:
            self.ejson[self.element_type]["user_data"] = self.user_data

        self.prepare_ejson()
        return self.ejson
