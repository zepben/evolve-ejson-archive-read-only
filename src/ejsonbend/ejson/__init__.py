"""
Copyright 2019 Zeppelin Bend Pty Ltd
This file is part of ejsonbend.

ejsonbend is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ejsonbend is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with ejsonbend.  If not, see <https://www.gnu.org/licenses/>.
"""


from typing import List
from abc import abstractmethod, ABCMeta
from json import JSONEncoder
from ejsonbend.ejson.ejson_node import EjsonNode
from collections import defaultdict

EJSON_UNITS_DEFAULT = {"current": 1.0, "voltage": 1.0, "power": 1.0, "impedance": 1.0, "length": 1.0}
EJSON_MEAS_UNITS_DEFAULT = {"current": 1.0, "voltage": 1.0, "power": 1.0, "impedance": 1.0, "length": 1.0}

PB_EJSON_PHASE_MAPPING = defaultdict(lambda: ['unknown'])
PB_EJSON_PHASE_MAPPING['A'] = ['r']
PB_EJSON_PHASE_MAPPING['B'] = ['w']
PB_EJSON_PHASE_MAPPING['C'] = ['b']
PB_EJSON_PHASE_MAPPING['AB'] = ['r', 'w']
PB_EJSON_PHASE_MAPPING['BC'] = ['w', 'b']
PB_EJSON_PHASE_MAPPING['AC'] = ['r', 'b']
PB_EJSON_PHASE_MAPPING['ABC'] = ['r', 'w', 'b']
CIM_PHS_TO_EJSON_WIRING = defaultdict(str)
CIM_PHS_TO_EJSON_WIRING['A'] = 'wye'
CIM_PHS_TO_EJSON_WIRING['B'] = 'wye'
CIM_PHS_TO_EJSON_WIRING['C'] = 'wye'
CIM_PHS_TO_EJSON_WIRING['AB'] = 'delta'
CIM_PHS_TO_EJSON_WIRING['BC'] = 'delta'
CIM_PHS_TO_EJSON_WIRING['AC'] = 'delta'
CIM_PHS_TO_EJSON_WIRING['ABC'] = 'delta'
CIM_PHS_TO_EJSON_WIRING['N'] = 'wye'

CIM_PHS_SHUNT_TO_EJSON_WIRING = defaultdict(str)
CIM_PHS_SHUNT_TO_EJSON_WIRING['D'] = 'delta'
CIM_PHS_SHUNT_TO_EJSON_WIRING['Y'] = 'wye'
CIM_PHS_SHUNT_TO_EJSON_WIRING['Yn'] = 'wye'
CIM_PHS_SHUNT_TO_EJSON_WIRING['I'] = 'single'
CIM_PHS_SHUNT_TO_EJSON_WIRING['G'] = 'unknown'


def get_ejson_cons(terminals):
    """
    Gets the ejson connections for a set of terminals. This is in the form of:
    [{"node":"node_id", "phs":[list of phase labels ], "closed": true},.. ]

    "closed" attribute is associated with terminals of the elements and is a single value that applies for all phases
    at the connection (terminal). True implies circuit is closed and energised, False implies open and de-energised,
    None implies lack of a switch or breaker associated with this Terminal.
    TODO: "closed" information allowed to be specified in a per-phase format like: "closed": [true,true,false]
    TODO: Support "dc": True/False according to spec

    :param terminals:
    :return:
    """
    ejson_cons = []
    for term in terminals:
        switch = term.get_switch()

        # If terminal has an associated switch/breaker, set closed by state of breaker, otherwise use
        # terminals connected state
        closed = not switch.is_open() if switch is not None else term.connected
        if not closed:
            if len(switch) > 1:
                closed = switch.open
        else:
            closed = None
        # if closed is True we actually set closed to None implying there is no switch (and thus leaving it out of the
        # ejson). Otherwise closed is False.
        # Note: There may actually be a switch or a breaker that *is* closed, but this is not important for ejson.
        ejson_cons.append(EjsonConnection(term.connectivity_node.mrid, PB_EJSON_PHASE_MAPPING[term.phases.phase],
                                          closed=closed))
    return ejson_cons


def get_ejson_node_phases(terminals):
    phases = []
    for terminal in terminals:
        for phase in PB_EJSON_PHASE_MAPPING[terminal.phases.phase]:
            if phase not in phases:
                phases.append(phase)
    return sorted(phases)


class EJSONEncoder(JSONEncoder):
    def default(self, o):
        try:
            return o.to_ejson_dict()
        except AttributeError as n:
            return super().default(o)


class EjsonConnection(object):

    def __init__(self, node_id: str, phases: List[str], closed: bool = None, dc: bool = None):
        self.node = node_id
        self.phs = phases
        self.closed = closed
        self.dc = dc

    def to_ejson_dict(self):
        ret = self.__dict__.copy()
        if self.closed or self.closed is None:
            del ret["closed"]
        if self.dc is None:
            del ret["dc"]
        return ret


class EjsonXY(object):
    def __init__(self, xy: List[float]):
        self.xy = xy

    def to_ejson_dict(self):
        return self.xy


class EjsonLatLong(object):
    def __init__(self, latlong: List[float]):
        self.latlong = latlong

    def __eq__(self, other):
        return self.latlong == other.latlong

    def to_ejson_dict(self):
        return self.latlong


class EjsonElement(object, metaclass=ABCMeta):

    def __init__(self, id, cons: List[EjsonConnection] = None, in_service: bool =  True, user_data: dict = None):
        self.element_id = id
        self.cons = cons
        self.element_type = 'Element'
        self.user_data = user_data
        self.in_service = in_service
        self.ejson = {}

    def prepare_ejson(self):
        assert len(self.cons) < 3
        self._add_cons()
        self._add_user_data()
        self.ejson[self.element_type]["in_service"] = self.in_service
        self.validate()

    @abstractmethod
    def validate(self):
        pass

    @abstractmethod
    def to_ejson_dict(self):
        pass

    def _add_cons(self):
        if self.cons is not None:
            self.ejson[self.element_type]["cons"] = list()

            for connection in self.cons:
                self.ejson[self.element_type]["cons"].append(connection.to_ejson_dict())

    def _add_user_data(self):
        if self.user_data is not None:
            self.ejson[self.element_type]["user_data"] = self.user_data
